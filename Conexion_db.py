import psycopg2

class Conexion_db:

    def __init__(self, host, data, user, pasw):
        try:
            self.conn = psycopg2.connect(
                host=host,
                database=data,
                user=user,
                password=pasw)
            # create a cursor
            self.cur = self.conn.cursor()
        except:
            print('Conexión no exitosa!')
    
    def consultar_db(self, query): # query: SELECT
        try:
            self.cur.execute(query)
            response = self.cur.fetchall()
            return response
            print('Petición query exitosa!')
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            return 'error'
    
    def escribir_db(self, query): # query: INSERT, DELETE, UPDATE
        try:
            self.cur.execute(query)
            self.conn.commit()
            print('Petición query exitosa!')
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            return 'error'

    def cerrar_db(self): # Cerrar la conexión
        self.cur.close()
        self.conn.close()
       