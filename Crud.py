from Conexion_db import Conexion_db

class Crud:

    def __init__(self, host, db, user, pasw):
        self.my_conn = Conexion_db(host, db, user, pasw)

    def cerrar_db(self):
        self.my_conn.cerrar_db()

    def insertar_pasajero(self, nombre, direcion, telefono, fecha_nacimiento):
        query = "INSERT INTO \"Pasajeros\" ("+\
            "nombre, direccion, telefono, fecha_nacimiento)"+\
            f"VALUES ('{nombre}', '{direcion}', '{telefono}', '{fecha_nacimiento}');"
        self.my_conn.escribir_db(query)

    def eliminar_pasajero(self, u_id):
        query = f"DELETE FROM \"Pasajeros\" WHERE id ={u_id};"
        self.my_conn.escribir_db(query)

    def actualizar_pasajero(self, col, valor, u_id):
        query = f"UPDATE \"Pasajeros\" set {col} = '{valor}' WHERE id ={u_id};"
        self.my_conn.escribir_db(query)

    def seleccionar_pasajero(self):
        query = "SELECT * FROM \"Pasajeros\";"
        return self.my_conn.consultar_db(query)
    
    def seleccionar_por_id(self, u_id):
        query = f"SELECT * FROM \"Pasajeros\" WHERE id= {u_id};"
        return self.my_conn.consultar_db(query)